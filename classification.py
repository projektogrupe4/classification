import os
import glob
import re
import jsonlines
import subprocess
from itertools import chain
from mungy import date_group
from pathlib import Path
import mpipe
os.system("pip install pandas")
os.system("pip install plotnine")
import pandas as pd
import plotnine as gg


def mk_cmd_fnames_to_vw(path_csvfnames, output=None):
    cmd = " | ".join([
        "cat {}".format(path_csvfnames),
        "mungy catcsv ",
        (
            'mungy csv2vw'
            ' --label=impression_was_clicked'
            ' --binary-label'
            ' --header'
            ' --all-categorical'
            ' --ignored-columns=log_time'
            ' --nested-val-delims=";,"'
        ) + (" --output {}".format(output) if output else "")
    ])
    return cmd


def mk_cmd_extract_column(path_csv_filenames):
    cmd = " | ".join([
        "cat {}".format(path_csv_filenames),
        "mungy catcsv",
        'mpipe extract-column impression_was_clicked --header --delim="\t"'
    ])
    return cmd


def mk_cmd_vw_train(path_model, path_readable_model, path_data=None, L1=None, L2=None):
    cmd = " ".join([
        "vw",
        "--normalized",
        "--passes 50",
        "--cache",
        "--data {}".format(path_data),
        "--final_regressor {}".format(path_model),
        "--readable_model {}".format(path_readable_model),
        "--l1 {}".format(L1) if L1 else "",
        "--l2 {}".format(L2) if L2 else "",
    ])
    return ("rm {}.cache; ".format(path_data) if path_data else "") + cmd


def mk_cmd_vw_predict(path_model, path_predictions, path_data=None):
    cmd = " ".join([
        "vw",
        "--normalized",
        "--testonly",
        "--cache",
        "--link=logistic",
        "--data {}".format(path_data) if path_data and path_data != "-" else "",
        "--initial_regressor {}".format(path_model),
        "--predictions {}".format(path_predictions),
    ])
    return ("rm {}.cache; ".format(path_data) if path_data else "") + cmd


def mk_cmd_classification_metrics(path_csv_filenames, path_predictions, output=None):
    cmd_extract_column = mk_cmd_extract_column(path_csv_filenames)
    cmd = " | ".join([
        cmd_extract_column,
        'paste -d " " - {}'.format(path_predictions),
        "mpipe metrics-cls" + (" --output {}".format(output) if output else "")
    ])
    return cmd


def mk_dir_data(dataset_id):
    cmd = " ".join([
        "mkdir",
        "-p",
        "{}".format(dataset_id)])
    return cmd


default_csv = glob.glob('.csv')
basepath = "home"
# Writes all files into text files with file names
all_filenames = glob.glob("../task_2/data_munging/ctr_by_country_by_hour/user_country_*.csv")
countryid = list()
for fname in all_filenames:
    res = re.findall("country_(\d+)_", fname)
    if not res: continue
    countryid.append(res)
    unique_country_id = list(set(chain.from_iterable(countryid)))
# Change base_path into whatever your parent directory is. (My original was python file path was
# '/home/vidmantas/PycharmProjects/projektas_RA2/classification.py'
base_path = "/home/vidmantas/PycharmProjects/"
for k in unique_country_id:
    os.chdir(base_path)
    all_filenames = glob.glob("task_2/data_munging/ctr_by_country_by_hour/user_country_{}*.csv".format(k))
    all_filename_pairs = date_group.make_datetime_filename_pairs(all_filenames)
    ds_train_4w_country = date_group.make_training_testing_datasets(
        all_filename_pairs,
        n_training_periods = 4,
        training_period_type = 'week',
        n_testing_periods = 1,
        testing_period_type = 'day',
        n_window_periods = 2,
        window_period_type = 'week',
        min_testing_datasets = 14
    )
    ds_train_4w_named_country = date_group.name_training_testing_datasets(ds_train_4w_country)
#TODO parasyt funkcija, kad automatiskai kurtu tuos ds_1, ds_2 - nespejom
    ds_1 = ds_train_4w_named_country[0]
    ds_2 = ds_train_4w_named_country[1]
    #this is the trick to make code swap the folders.
    path_swapper = "/home/vidmantas/PycharmProjects/task_2/country{}".format(k)
    os.system("mkdir -p {}".format(path_swapper))
    print(path_swapper)
    os.chdir(path_swapper)
    os.system("mkdir -p train_test_fnames/train_4w_test_14d_by_2w")

    def mk_dir_output(dataset_index):
        return os.path.join(
            "train_test_fnames",
            "train_4w_test_14d_by_2w",
            "{:03d}".format(dataset_index))
    for i, ds in enumerate(ds_train_4w_named_country):

        dir_output = mk_dir_output(i)

        os.makedirs(dir_output, exist_ok=True)

        fname_train = "train__{}__{}.txt".format(
            ds.train_dataset.interval.start.strftime("%Y-%m-%d_%H%M"),
            ds.train_dataset.interval.end.strftime("%Y-%m-%d_%H%M"))

        path_train = os.path.join(dir_output, fname_train)

        with open(path_train, "w") as f:
            print("writing train filenames to {}".format(path_train))
            f.write("\n".join(sorted(ds.train_dataset.filepaths)))

        for j, test_ds in enumerate(ds.test_datasets[:14]):
            fname_test = "test_{:03d}__{}__{}.txt".format(
                j,
                ds.train_dataset.interval.start.strftime("%Y-%m-%d_%H%M"),
                ds.train_dataset.interval.end.strftime("%Y-%m-%d_%H%M"))

            path_test = os.path.join(dir_output, fname_test)

            with open(path_test, "w") as f_test:
                print("writing test filenames to {}".format(path_test))
                f_test.write("\n".join(sorted(test_ds.filepaths)))
    os.chdir("/home/vidmantas/PycharmProjects/")

    #Creating directories for data storing
    country_id = k
    for path in Path().rglob("task_2/country{}/train_test_fnames/train_4w_test_14d_by_2w/*/".format(k)):
        unique_dataset_id = os.path.split(path)[1]
        pathz = os.path.join(
            "task_2",
            "country{}".format(country_id),
            "train_test_data",
            "{}".format(unique_dataset_id))
        dataa = mk_dir_data(pathz)
        subprocess.run(dataa, shell=True, executable="/bin/bash", check=True)
        pathz_modelling_by_country_by_id = os.path.join(
            "task_2",
            "country{}".format(k),
            "models",
            "{}".format(unique_dataset_id)
        )
        modelling_dir = mk_dir_data(pathz_modelling_by_country_by_id)
        subprocess.run(modelling_dir, shell=True, executable="/bin/bash", check=True)
    #Writing data into directories
        iterator = Path().rglob("task_2/country{}/train_test_fnames/train_4w_test_14d_by_2w/*/*".format(k))
    for path in iterator:
        unique_dataset_id = os.path.split(os.path.split(path)[0])[1]
        path_data_ = os.path.join(
            "task_2",
            "country{}".format(k),
            "train_test_data",
            unique_dataset_id,
            Path(os.path.split(path.with_suffix('.vw'))[1]))
        creating_files_with_data = mk_cmd_fnames_to_vw(path, path_data_)
        subprocess.run(creating_files_with_data,
                       shell=True)

    L1s = [1e-6, 1e-7,1e-8]
    L2s = [1e-6, 1e-7,1e-8]
    #Training data, predicting on test data sets, computing metrics, plotting metrics and saving plots into disk.
    for i in L1s:
        for j in L2s:
            for root in Path().rglob("task_2/country{}/models/*/".format(k)):
                root_model = Path.joinpath(
                    root,
                    "model_L1-{}_L2-{}".format(i, j)
                )
                path_readable_model = os.path.join(
                    root_model,
                    "readable_model.txt"
                )
                path_predictions = os.path.join(
                    root_model,
                    "predictions.txt"
                )
                path_data_training = glob.glob(os.path.join(
                    "task_2",
                    "country{}".format(k),
                    "train_test_data",
                    os.path.split(root)[1],
                    "train*"
                ))
                path_metrics = os.path.join(
                    root_model,
                    "metrics.JSON"
                )
                path_model = os.path.join(
                    root_model,
                    "model.vw"
                )
                mk_models_dir = mk_dir_data(root_model)
                subprocess.run(
                    mk_models_dir,
                    shell=True, executable="/bin/bash", check=True
                )

                cmd_train_vw_model = mk_cmd_vw_train(
                    path_model,
                    path_readable_model,
                    path_data_training[0],
                    i,
                    j
                )
                subprocess.run(cmd_train_vw_model,
                               shell=True, executable="/bin/bash", check=True)

                test_files_pattern = os.path.join(
                    "task_2",
                    "country{}".format(k),
                    "train_test_fnames",
                    "train_4w_test_14d_by_2w",
                    os.path.split(root)[1],
                    "test*"
                )
                test_file_data = sorted(glob.glob(test_files_pattern))
                for w, test_fname in enumerate(test_file_data, start=1):
                    print("handling day {:0d}, test_period: {}".format(w, test_fname))

                    path_predictions = os.path.join(
                        root_model,
                        "predictions_day_{:02d}.txt".format(w)
                    )

                    cmd_fnames_to_vw_test_day = mk_cmd_fnames_to_vw(test_fname)

                    cmd_predict_from_stdin = mk_cmd_vw_predict(
                        path_model,
                        path_predictions)

                    cmd_predict_test_day = "{} | {}".format(
                        cmd_fnames_to_vw_test_day,
                        cmd_predict_from_stdin)
                    metrics_root = os.path.join(
                        root_model,
                        "metrics"
                        )
                    os.system("mkdir -p {}".format(metrics_root))
                    path_metrics = os.path.join(
                        metrics_root,
                        "metrics__day_{:02d}.txt").format(w)
                    cmd_metrics_day = mk_cmd_classification_metrics(test_fname, path_predictions, path_metrics)
                    subprocess.run(cmd_predict_test_day, shell=True, executable="/bin/bash", check=True)
                    subprocess.run(cmd_metrics_day, shell=True, executable="/bin/bash", check=True)
                metrics_all_path = os.path.join(
                    root_model,
                    "all_metrics.txt"
                )
                print(metrics_all_path)
                os.system("ls {}/metrics/* | \
                    sort | mungy catcsv > {}".format(root_model,metrics_all_path))
                metrics_df = pd.read_csv(
                    "{}".format(metrics_all_path))
                metrics_df["day"] = (metrics_df.index + 1)
                metrics_long_df = pd.melt(
                    metrics_df,
                    id_vars=["day"],
                    value_vars=["precision", "recall", "f-measure", "auc"],
                    var_name="variable")

                print(metrics_long_df)
                plot_path = Path.joinpath (
                    root,
                    "plots"
                )
                os.system("mkdir -p {}".format(plot_path))
                metrics_plot = (
                        gg.ggplot(metrics_long_df, gg.aes(x="day", y="value", color="variable")) +
                        gg.geom_line() +
                        gg.xlab("$N$ days after training") +
                        gg.ggtitle("Metrics by testing day L1_{} L2_{}".format(i, j)) +
                        gg.ylim(0, 1) +
                        gg.theme_bw()
                )
                plot_name = os.path.join(
                    plot_path,
                    "metrics__all__l1_{}_l2_{}.png".format(i, j)
                )
                metrics_plot.save(plot_name)


